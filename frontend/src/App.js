import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios, {post} from 'axios';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'
import {browserHistory} from "react-router";



class Login extends React.Component {

  handleSignIn(e) {
    e.preventDefault()
    let email = this.refs.email.value
    let password = this.refs.password.value
    this.props.checkUserInfo(email, password)
  }
  
  render() { 
    return (
        <div>
          <form onSubmit={this.handleSignIn.bind(this)} className="form-horizontal">
            <div className="form-group">
              <div className="col-sm-10">
                <input 
                  type="email" 
                  className="form-control" 
                  id="inputEmail3" 
                  placeholder="Email" 
                  ref = "email"
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-10">
                <input 
                  type="password" 
                  className="form-control" 
                  id="inputPassword3" 
                  placeholder="Password"
                  ref = "password"
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-10">
                <div className="checkbox">
                  <label>
                    <input type="checkbox" /> Remember me
                  </label>
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-10">
                <input type="submit" 
                className="btn btn-primary"
                value="login" />
              </div>
            </div>
          </form>
        </div>
      )
  }
}



class LoginLayout extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      email: '',
      password: ''
    };  
  } 

  componentWillMount() {
    let isUserLoggedIn = false;
    if(sessionStorage.getItem('myData') !== 'null'){
      isUserLoggedIn = true;
    }
    isUserLoggedIn ? this.props.history.push('/page') : this.props.history.push('/')
  }

  checkUserInfo = (username, password) => {
    this.setState({
      user: {
        username,
        password,
      }
    });

    let temp = {
      email: username,
      password: password
    }
    axios.post('http://localhost:4000/users/login', temp).then((res) => {
      this.props.history.push('/page');
      sessionStorage.setItem('myData', res.data._id);
    }).catch(err => {
      console.log("error");
    })
  }

  handleChange = () => {
    console.log("atlease")
  }

  render() {
    return (
      <div>
        <h1 className="heading">Login Application</h1>
        <div className="container App">
          <div className="layoutStyle">
            <Login checkUserInfo={this.checkUserInfo.bind(this)} handleChange={this.handleChange.bind(this)} email={this.state.email} password={this.state.password}/>
          </div>
        </div>
      </div>
    )
  }
}


class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <div>
            <Route exact path="/" component={LoginLayout}/>
            <Route path="/page" component={Page}/>
          </div>
        </Router>
      </div>
  );
}
}

class Page extends Component {
  
  logmeout = () => {
    sessionStorage.setItem('myData', null);
    this.props.history.push('/');
  }

  render() {
    return (
      <div>
        <h4 className="heading">Welcome to Excel Application</h4>
        <button className="btn btn-primary logoutStyle" onClick={this.logmeout}> Log out </button>
        <TableComponent />
      </div>
    );
  }
}

class TableComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      file:null,
      fileResponseRecieved: false
    }
    this.uploadFile= this.uploadFile.bind(this);
  }
  
  uploadFile(e) {
    console.log(this,"firsttjois")
      const file = e.target.files[0];
      const url = 'http://localhost:4000/files';
      const formData = new FormData();
      formData.append('file',file)
      const config = {
          headers: {
              'content-type': 'multipart/form-data'
          }
      }
      return post(url, formData,config)
      .then(res => {
        console.log(res.data, this);
        this.dataToSend = res.data;
        this.setState({
          fileResponseRecieved: true
        }); 
      }).catch(err => {
        console.log(err, "<= err");
      })
  }

    render() {
      return (
        <div className="fileContainer">
          <h5 className="labelStyle">Please Select the xls or csv file which you want to import :</h5>
          <input className="fileStyle" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" type="file" onChange={this.uploadFile} />
          {this.state.fileResponseRecieved ? <RenderedTableComponent data={this.dataToSend} /> : null }
        </div>
      )
    } 
}

class RenderedTableComponent extends Component {
  constructor(props) {
    super(props);
  }

  getHeaders = function() {
    var rowContent = this.props.data.data;
    let lengthOfObject;
    let maxlengthOfObject = 0;
    let maxLengthObj;
    rowContent.map(function(colData) {
      lengthOfObject = Object.keys(colData).length;
      if(lengthOfObject > maxlengthOfObject){
        maxlengthOfObject = lengthOfObject;
        maxLengthObj = colData;
      }
    });
    let headerData = Object.keys(maxLengthObj)
    return headerData.map(function(data) {
      return <th key={data}> {data}</th>
    });
  }

  getData = function() {
    var rowContent = this.props.data.data;
    return rowContent.map(function(colData){
      let contentData = Object.values(colData)
    console.log("comig here",contentData);      
      return <tr>{<FinalVal contentData = {contentData}/>}<td><button>Edit</button><button>Delete</button></td></tr>
    })
  }

  render(){

    var headerComponent = this.getHeaders();
    var dataComponent = this.getData();

    return (  
        <table className="table table-bordered tableStyle">
          <thead>
            <tr>
              {headerComponent}
              <th width="120px">Actions</th>
            </tr>
          </thead>
        <tbody>
              {[dataComponent]}
        </tbody>
      </table>
    )
  }
}

class FinalVal extends Component {
  constructor(props) {
    super(props);
  };

  getFinalValues() {
    let finVal = this.props.contentData;
    console.log(finVal,"asd");
    return finVal.map(function(endValues) {
      console.log("endValue", endValues)
      return <td key={endValues}>{endValues}</td>     
    })
  }

  render(){
    var getFinalValues = this.getFinalValues();
    return(
      [getFinalValues]
    );
  }
}

export default App;

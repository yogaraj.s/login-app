var express = require('express');
var router = express.Router();
var multer = require('multer');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/**
 ... express.js boilerplate
 routes, middlewares, helpers, loggers, etc
**/

// configuring Multer to use files directory for storing files
// this is important because later we'll need to access file path
const storage = multer.diskStorage({
  destination: './files',
  filename(req, file, cb) {
    cb(null, `${new Date()}-${file.originalname}`);
  },
});

const upload = multer({ storage });

// express route where we receive files from the client
// passing multer middleware
router.post('/files', upload.single('file'), (req, res) => {
 const file = req.file; // file passed from client
 const meta = req.body; // all other values passed from the client, like name, etc..
 console.log(req.file);
 (req.file.originalname.includes('xl')) ? convertToXls(req, res) : convertToCsv(req)
 .then(result => {
    return res.json(result);
 }).catch(error => {
   return res.json(error);
 })
 //return res.json(req.file);
});

const convertToCsv = function (req, res) {
  return new Promise((resolve, reject) => {
    /** csv file
    a,b,c
    1,2,3
    4,5,6
    */
    const csvFilePath = req.file.destination.toString() + '/' + req.file.filename.toString();
    const csv = require('csvtojson');
    var arr = [];
    csv()
    .fromFile(csvFilePath)
    .on('json',(jsonObj)=>{
      /* resolve({
        status: 200,
        data: jsonObj
      }) */
      console.log(jsonObj);
      arr.push(jsonObj);
      console.log(arr);
        // combine csv header row and csv line to a json object
        // jsonObj.a ==> 1 or 4
    })
    .on('done',(error)=>{
        resolve({
          status: 200,
          data: arr
        });
        console.log(error);
    })
  });
}

const convertToXls = function (req, res) {
  xlsxj = require("xlsx-to-json");
  xlsxj({
    input: req.file.destination.toString() + '/' + req.file.filename.toString(), 
    output: "output.json"
  }, function(err, result) {
    if(err) {
      return res.status(500).json(err);
    }else {
      return res.status(200).json(result);
    }
  }); 
}

module.exports = router;
